package litsteam;


import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;


public class Main {
    public static void main(String[] args) {
        ObjectMapper mapper = new ObjectMapper();

        Workers myWorkers = null;

        try {
            myWorkers = mapper.readValue(new File("testdata/workers.json"), Workers.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(myWorkers !=null)
        myWorkers.sortWorkers();

    }

}
