package litsteam;

public class Worker {

    public String surname;
    public String position;
    private int salary;

    public Worker() {
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Worker{" +
                "surname='" + surname + '\'' +
                ", position='" + position + '\'' +
                ", salary=" + salary +
                '}'+"\n";
    }
}
