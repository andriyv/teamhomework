package litsteam;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Workers {
    private ArrayList<Worker> workers;

    public Workers() {
    }

    public ArrayList<Worker> getWorkers() {
        return workers;
    }

    public void setWorkers(ArrayList<Worker> workers) {
        this.workers = workers;
    }

    @Override
    public String toString() {
        return "Workers{" +
                "workers=" + workers +
                '}';
    }

    public void sortWorkers() {
        Collections.sort(workers, new Comparator<Worker>() {
            @Override
            public int compare(Worker w1, Worker w2) {
                if (w1.getSurname().compareTo(w2.getSurname()) == 0) {
                    if (w1.getPosition().compareTo(w2.getPosition()) == 0) {
                        return w1.getSalary() - w2.getSalary();
                    } else {
                        return w1.getPosition().compareTo(w2.getPosition());
                    }

                }
                return w1.getSurname().compareTo(w2.getSurname());
            }
        });
        System.out.println(workers);
    }
}
